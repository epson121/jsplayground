// closure -
//      wrap a function which is automatically invoked
//      call the returned function with parameters
//      no need to create data array every time! 
var get = (function (n) {
    var data = [1, 2, 3, 4, 5, 6, 7, 8];

    return function(n) {
        return data[n];
    }
})();

console.log(get(4));
