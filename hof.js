"use strict";
eval("var a = false;");
//print(typeof a);
//returns an error, as a can't be defined in eval
//dueto 'use strict'
//
// higher order functions
// filter
//

var data = [1, 2, 3, 4, 5, 6, 7, 8];

var isEven = function(element) {
    return element % 2 === 0;
}

var isOdd = function(element) {
    return element % 2 !== 0;
}

var evens = data.filter(isEven);
var odds = data.filter(isOdd);

console.log(evens);
console.log(odds);

// same thing could be achieved with
// reject
// EcmaScript 6 only

//var evens_2 = data.reject(isOdd);
//var odds_2 = data.reject(isEven);
//console.log(evens_2);
//console.log(odds_2);
//
//
// map
// apply some function to each element in list

var double = function(element) {
    return element * 2;
}

var halves = function(element) {
    return element / 2;
}

var doubles = data.map(double);
var halves = data.map(halves);
console.log(doubles);
console.log(halves);




// every
// checks wether all elements pass the test

var positive = function(element) {
    return element > 0;
}

var positives = data.every(positive);
console.log(positives);
// reusability of functions
console.log(data.every(isOdd));

// reduce

var sum = function(previous, current) {
    return previous + current;
}
console.log(data.reduce(sum));

















